import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import * as firebase from 'firebase';

import reducers from '../src/reducers/index';
import SideBar from './SideBar'

import 'antd/dist/antd.css';
import './App.css'

class App extends Component {

  componentWillMount() {

    const config = {
      apiKey: "AIzaSyCjQA8UT0EHkyye2nax6-6LO4PGg-LDTIc",
      authDomain: "easypay-a5c8c.firebaseapp.com",
      databaseURL: "https://easypay-a5c8c.firebaseio.com",
      projectId: "easypay-a5c8c",
      storageBucket: "easypay-a5c8c.appspot.com",
      messagingSenderId: "13406958261"
    };

    firebase.initializeApp(config);

  }

  render() {

    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
        <SideBar />
      </Provider>
    );
  }
}

export default App;