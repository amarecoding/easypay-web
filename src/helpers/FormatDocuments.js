/**
|--------------------------------------------------
| FORMAT DOCUMENTS FIREBASE
|--------------------------------------------------
*/

export const onFormatCategorysMenu = (snapshot) => {

    let categorysMenu = [];
    snapshot.forEach(row => {
        categorysMenu.push({
            'key': row.id,
            'nameCategory': row.data().nameCategory,
        });
    });

    return categorysMenu;
}

export const onFormatMenusFood = (snapshot) => {

    let menuFoood = [];
    snapshot.forEach(row => {
        menuFoood.push({
            'key': row.id,
            'categoryMenu': row.data().categoryMenu,
            'ingredients': row.data().ingredients,
            'price': row.data().price,
            'saucer': row.data().saucer
        });
    });

    return menuFoood;
}

export const onFormatListWaiters = (snapshot) => {

    let waiters = [];
    snapshot.forEach(row => {
        waiters.push({
            'key': row.id,
            'name': row.data().name,
            'restaurant': row.data().restaurant,
            'tableNumber': row.data().tableNumber
        });
    });

    return waiters;
}

export const onFormatListRest = (snapshot) => {

    let restaurants = [];
    snapshot.forEach(row => {
        restaurants.push({
            'key': row.id,
            'name': row.data().name,
            'direction': row.data().direction,
            'telephone': row.data().telephone
        });
    });

    return restaurants;
}

export const onFormatLisTable = (snapshot) => {

    let tables = [];
    snapshot.forEach(row => {
        tables.push({
            'key': row.id,
            'restaurant': row.data().restaurant,
            'restaurantkey': row.data().restaurantkey,
            'zone': row.data().zone,
            'zonekey': row.data().zonekey,
            'listTableNumber': row.data().listTableNumber
        });
    });

    return tables;
}