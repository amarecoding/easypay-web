import firebase from 'firebase';

export const CheckLoginUser = () => {

    return new Promise((resolve, reject) => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                resolve(user);
            } else {
                reject()
            }
        })
    })
}

export const Create = (collection, params) => {

    return new Promise((resolve, reject) => {

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true })

        db.collection(collection).add(params).then((response) => {
            resolve(response);
        }).catch((error) => {
            reject(error);
        });

    });
}

export const Update = (collection, key, params) => {

    return new Promise((resolve, reject) => {

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true });

        let myCollection = db.collection(collection).doc(key);
        myCollection.update(params).then(snapshot => {
            resolve(snapshot);
        }).catch(error => {
            reject(error);
        })

    });

}

export const Delete = (collection, key) => {

    return new Promise((resolve, reject) => {

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true })
        db.collection(collection).doc(key).delete().then((snapshot) => {
            resolve(snapshot);
        }).catch(error => {
            reject(error);
        })

    })
}

export const List = (collection) => {

    return new Promise((resolve, reject) => {

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true })
        let docRefs = db.collection(collection);

        docRefs.get().then((snapshot) => {
            resolve(snapshot);
        }).catch(error => {
            reject([]);
        });

    });
}

export const Get = (collection, key) => {

    return new Promise((resolve, reject) => {

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true });

        let docRef = db.collection(collection).doc(key);

        docRef.get().then(snapshot => {
            resolve(snapshot);
        }).catch(error => {
            reject(error);
        })

    });

};

