/**
|--------------------------------------------------
| ACTIONS CREATORS MESAS
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_MESAS,
    ON_CALL_DATA_SELECT,
    ON_LOAD_DATA_TABLES,
    ON_LOAD_TABLES,
} from './types'

import { onFormatNotification, Notification } from '../../components/ListComponents';
import * as Firebasehelper from '../../helpers/Firebasehelper';
import { onFormatListRest, onFormatLisTable } from '../../helpers/FormatDocuments';
import firebase from 'firebase';

const collectionrestaurant = 'restaurants';
const collectionTables = 'tables';

export const UpdateInputTables = ({ prop, value }) => {
    return {
        'type': UPDATE_INPUT_MESAS,
        'payload': { prop, value }
    }
}

export const onCallDataSelect = () => {

    return (dispatch) => {

        Firebasehelper.List(collectionrestaurant).then((snapshot) => {
            let restaurants = onFormatListRest(snapshot);
            let zones = onListZones();
            dispatch({ type: ON_CALL_DATA_SELECT, payload: [restaurants, zones] });
        });
    }
}

export const onListTables = () => {

    return (dispatch) => {

        dispatch({ type: ON_LOAD_TABLES, payload: true });

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true });

        db.collection(collectionTables).onSnapshot((snapshot) => {
            let tables = onFormatLisTable(snapshot);
            dispatch({ type: ON_LOAD_DATA_TABLES, payload: tables });
            dispatch({ type: ON_LOAD_TABLES, payload: false });

        }, (error) => {
            console.log(error)
        });
    }
}

export const createTable = (table) => {

    return (dispatch) => {

        Firebasehelper.Create(collectionTables, table).then(snapshot => {
            let formatNoti = onFormatNotification('Pefecto', 'Haz creado un nuevo mesero....', true)
            Notification(formatNoti);
        }).catch(error => {
            console.log(error);
        });
    }
}

export const updateTable = (key, params) => {

    return (dispatch) => {
        Firebasehelper.Update(collectionTables, key, params).then(snapshot => {
            let formatNoti = onFormatNotification('Pefecto', 'Haz editado un mesero....', true)
            Notification(formatNoti);
        }).catch(error => {
            console.log(error);
        });
    }
}

export const deleteTable = (props) => {

    return (dispatch) => {

        let key = props.key
        Firebasehelper.Delete(collectionTables, key).then(snapshot => {
            console.log(snapshot);
        }).catch(error => {
            console.log(error);
        })
    }
}

const onListZones = () => {
    return [
        { name: 'Planta Baja', key: 'PlantaBaja' },
        { name: 'Planta Alta', key: 'PlantaAlta' },
        { name: 'Exterior', key: 'Exterior' }
    ]
}