import {
    UPDATE_INPUT_MESAS,
    ON_CALL_DATA_SELECT,
    ON_LOAD_TABLES,
    ON_LOAD_DATA_TABLES
} from './types';

const INIT_STATE = {
    restaurant: '',
    restaurants: [],
    zone: '',
    zones: [],
    tableNumber: '',
    loading: false,
    tables: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case UPDATE_INPUT_MESAS:
            return { ...state, [action.payload.prop]: action.payload.value };
        case ON_CALL_DATA_SELECT:
            return { ...state, restaurants: action.payload[0], zones: action.payload[1] };
        case ON_LOAD_TABLES:
            return { ...state, loading: action.payload }
        case ON_LOAD_DATA_TABLES:
            return { ...state, tables: action.payload }
        default:
            return state;
    }
}