import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Input, Button, Select } from 'antd';

import { UpdateInputTables, onCallDataSelect, createTable, updateTable } from './Actions';

const Option = Select.Option;
const FormItem = Form.Item;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class MesasForm extends Component {

    state = {
        formLayout: 'horizontal',
        keydocument: null
    };

    componentDidMount() {
        this.props.onCallDataSelect();
        this.props.provideController({
            onClearForm: this.onClearForm,
            onLoadSetFields: this.onLoadSetFields,
        })
    }

    componentWillUnmount() {
        this.props.provideController(null);
    }

    onLoadSetFields = (row) => {
        if (row) {
            this.setState({ keydocument: row.key })
            this.props.form.setFieldsValue(
                {
                    restaurant: `${row.restaurant}-${row.restaurantkey}`,
                    zone: `${row.zone}-${row.zonekey}`,
                    listTableNumber: row.listTableNumber
                });
        }
    }

    onLoadOptionsSelect = () => {

        if (this.props.restaurants) {

            return this.props.restaurants.map((value, index) => {
                return (
                    <Option key={index} value={`${value.name}-${value.key}`}>{value.name}</Option>
                );
            });
        };
    }

    onLoadOptionsSelectZones = () => {
        if (this.props.zones) {

            return this.props.zones.map((value, index) => {
                return (
                    <Option key={index} value={`${value.name}-${value.key}`}>{value.name}</Option>
                );
            });
        };
    }

    UpdateInputTables = (prop, key) => {
        let value = (key.target === undefined) ? key : key.target.value;
        this.props.UpdateInputTables({ 'prop': prop, 'value': value });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {

            if (!err) {

                let table = this.onFormatDataTable(values);

                if (this.props.isNewTable) {
                    this.props.createTable({ ...table });
                    this.onClearForm();
                } else {
                    let keydocument = this.state.keydocument;
                    this.props.updateTable(keydocument, { ...table });
                }
            }
        });
    }

    onFormatDataTable = (values) => {

        let restaurant = values.restaurant.split('-')[0];
        let restaurantkey = values.restaurant.split('-')[1];

        let zone = values.zone.split('-')[0];
        let zonekey = values.zone.split('-')[1];

        return {
            restaurant: restaurant,
            restaurantkey: restaurantkey,
            zone: zone,
            zonekey: zonekey,
            listTableNumber: values.listTableNumber
        }
    }

    onClearForm = () => {
        this.props.form.resetFields()
        this.props.form.validateFields();
    }

    render() {

        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
        const restaurantError = isFieldTouched('restaurant') && getFieldError('restaurant');
        const zoneError = isFieldTouched('zone') && getFieldError('zone');
        const tableNumberError = isFieldTouched('listTableNumber') && getFieldError('listTableNumber');

        const { formLayout } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 6 },
            wrapperCol: { span: 16 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
            wrapperCol: { span: 16, offset: 6 },
        } : null;

        return (
            <div>

                <p className="margin-left">Completa todos los campos.</p>

                <Form layout={formLayout} onSubmit={this.handleSubmit}>
                    <FormItem
                        label="Restaurante"
                        {...formItemLayout}
                        validateStatus={restaurantError ? 'error' : ''}
                        help={restaurantError || ''}>
                        {getFieldDecorator('restaurant', {
                            rules: [{ required: true, message: 'El campo restaurante es requerido!' }],
                            onChange: (e) => this.UpdateInputTables('restaurant', e),
                        })(
                            <Select>
                                {this.onLoadOptionsSelect()}
                            </Select>
                        )}

                    </FormItem>

                    <FormItem
                        label="Zonas"
                        {...formItemLayout}
                        validateStatus={zoneError ? 'error' : ''}
                        help={zoneError || ''}>
                        {getFieldDecorator('zone', {
                            rules: [{ required: true, message: 'El campo zonas es requerido!' }],
                            onChange: (e) => this.UpdateInputTables('zone', e),
                        })(
                            <Select>
                                {this.onLoadOptionsSelectZones()}
                            </Select>
                        )}

                    </FormItem>

                    <FormItem
                        label="Numero de Mesa"
                        {...formItemLayout}
                        validateStatus={tableNumberError ? 'error' : ''}
                        help={tableNumberError || ''}>
                        {getFieldDecorator('listTableNumber', {
                            rules: [{ required: true, message: 'El campo numero de tabla es requerido!' }],
                            onChange: (e) => this.UpdateInputTables('tableNumber', e)
                        })(
                            <Input />
                        )}
                    </FormItem>


                    <FormItem {...buttonItemLayout}>
                        <Button
                            type="primary"
                            htmlType="submit"
                            disabled={hasErrors(getFieldsError())}>
                            {this.props.isNewTable ? 'Registrar' : 'Editar'}
                        </Button>
                    </FormItem>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { restaurant, restaurants, zones, zone } = state.Mesas;
    return { restaurant, restaurants, zones, zone };
}

const WrappedNewMesasForm = Form.create()(MesasForm);

export default connect(mapStateToProps, {
    UpdateInputTables,
    onCallDataSelect,
    createTable,
    updateTable
}, null, { withRef: true })(WrappedNewMesasForm);