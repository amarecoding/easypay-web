import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Drawer, Popconfirm, Divider, Button, Tooltip, List } from 'antd';
import { onListTables, deleteTable } from './Actions';
import QRCode from 'qrcode.react';

import MesasForm from './MesasForm';

const { Column } = Table;

class Mesas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            visibleDrawerQR: false,
            isNewTable: true,
            table: [],
            rest: "",
            zone: ""
        };
    }

    componentWillMount = () => {
        this.props.onListTables();
    }

    onNewTable = () => {
        this.setState({ visible: true, isNewTable: true });
        setTimeout(() => {
            this.tableController.onClearForm()
        }, 100)
    }

    onEditWaiter = (row, e) => {
        this.setState({ visible: true, isNewTable: false });
        setTimeout(() => {
            this.tableController.onLoadSetFields(row)
        }, 100)
    }

    onDeleteTables = (row) => {
        this.props.deleteTable(row)
    }

    onCloseDrawer = () => {
        this.setState({ visible: false });
    }

    onViewQRMesas = (row) => {
        this.setState({ visibleDrawerQR: true, table: [row], rest: row.restaurant, zone: row.zone });
    }

    onCloseDrawerQR = () => {
        this.setState({ visibleDrawerQR: false });
    }

    onRenderItem = (row) => {
        console.log(row)
        let numberTables: Array<string> = row.listTableNumber.split(',');
        let restkey = row.restaurantkey;
        let zone = row.zone;
        return (

            numberTables.map((value, index) => {
                return (
                    <List.Item>
                        <p className="text-center">{`Mesa No. ${value}`}</p>
                        <QRCode value={`https://easypay.com/?restket=${restkey}&zone=${zone}&numbertable=${value}`} />
                    </List.Item>
                )
            })
        )
    }

    render() {
        return (
            <div>
                <div className="main-container-con-margin">
                    <div className="action-module">
                        <p>Lista de Mesas.</p>
                        <Tooltip placement="left" title="Crear nuevas mesas">
                            <Button className="margin-botton-1em" type="primary" shape="circle" icon="plus" onClick={this.onNewTable.bind(this)} />
                        </Tooltip>
                    </div>
                    <Table
                        dataSource={this.props.tables}
                        loading={this.props.loading}
                        locale={{ emptyText: 'Por el momento no existe ningúna mesa registrada.' }}>
                        <Column
                            title="Restaurante"
                            dataIndex="restaurant"
                            key="restaurant"
                        />
                        <Column
                            title="Zona"
                            dataIndex="zone"
                            key="zone"
                        />
                        <Column
                            title="Número de Mesas"
                            dataIndex="listTableNumber"
                            key="listTableNumber"
                        />
                        <Column
                            title="Acciones"
                            key="actions"
                            render={(text, row) => (
                                <span>
                                    <a href="javascript:void(0)" onClick={(e) => this.onViewQRMesas(row, e)}>QR</a>
                                    <Divider type="vertical" />
                                    <a href="javascript:void(0)" onClick={(e) => this.onEditWaiter(row, e)}>Editar</a>
                                    <Divider type="vertical" />
                                    <Popconfirm
                                        title={`¿Seguro que quieres eliminar estas mesas?`} onConfirm={() => this.onDeleteTables(row)}
                                        okText="Sí" cancelText="No" >
                                        <a href="javascript:void(0)">Eliminar</a>
                                    </Popconfirm>
                                </span>
                            )}
                        />
                    </Table>
                    <Drawer
                        title={this.state.isNewTable ? "Crear mesas" : "Editar Mesas"}
                        placement="right"
                        closable={false}
                        width={640}
                        onClose={this.onCloseDrawer}
                        visible={this.state.visible}>
                        <MesasForm
                            isNewTable={this.state.isNewTable}
                            provideController={controller => this.tableController = controller} />
                    </Drawer>
                    <Drawer
                        title={`${this.state.rest} | Zona: ${this.state.zone}`}
                        placement="right"
                        closable={false}
                        width={640}
                        onClose={this.onCloseDrawerQR}
                        visible={this.state.visibleDrawerQR}>
                        <List
                            itemLayout="horizontal"
                            grid={{ column: 4 }}
                            dataSource={this.state.table}
                            renderItem={this.onRenderItem.bind(this)} />
                    </Drawer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { loading, tables } = state.Mesas;
    return { loading, tables };
}

export default connect(mapStateToProps, {
    onListTables,
    deleteTable
})(Mesas);