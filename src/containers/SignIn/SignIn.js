import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Checkbox, Card } from 'antd';
import { updateInputSignIn, onSignIn } from './Actions';
const FormItem = Form.Item;

class SignIn extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const { email, password } = this.props;
                this.props.onSignIn({ email, password });
            }
        });
    }

    onGoToDash = () => {
        if (this.props.goToDash) {
            setTimeout(() => {
                this.props.history.push('/dashboard');
            }, 100);
        }
    }

    updateInputSignIn = (prop, key) => {
        let value = key.target.value;
        this.props.updateInputSignIn({ 'prop': prop, 'value': value });
    }

    render() {

        const { getFieldDecorator } = this.props.form;

        return (
            <div className="onlyFormContainer">
                <div className="onlyFormSubContainer">
                    <Card title="Iniciar Sesion" extra={<a href="#/registrar">Registrarse</a>}>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <FormItem>
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: 'Completa el camplo email!' }],
                                    onChange: (e) => this.updateInputSignIn('email', e)
                                })(
                                    <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Completa el campo Password!' }],
                                    onChange: (e) => this.updateInputSignIn('password', e)
                                })(
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('remember', {
                                    valuePropName: 'checked',
                                    initialValue: true,
                                })(
                                    <Checkbox>Recordar</Checkbox>
                                )}
                                <a className="login-form-forgot" href="">¿Olvidaste tu password?</a>
                                <Button type="primary" htmlType="submit" className="login-form-button" loading={this.props.loadingButton}>
                                    Entrar
                                </Button>
                            </FormItem>
                            {this.onGoToDash()}
                        </Form>
                    </Card>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { email, password, goToDash, loadingButton } = state.SignIn;
    return { email, password, goToDash, loadingButton };
}

const WrappedLoginForm = Form.create()(SignIn);

export default connect(mapStateToProps, {
    updateInputSignIn,
    onSignIn
})(WrappedLoginForm);