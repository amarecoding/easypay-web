import {
    UPDATE_INPUT_SIGNIN,
    GO_TO_DASHBOARD,
    ON_LOADING_BUTTON
} from './types';

const INIT_STATE = {
    'email': '',
    'password': '',
    'loadingButton': false,
    'goToDash' : false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case UPDATE_INPUT_SIGNIN:
            return { ...state, [action.payload.prop]: action.payload.value };
        case ON_LOADING_BUTTON:
            return {...state , loadingButton : action.payload}
        case GO_TO_DASHBOARD: 
            return { ...state, goToDash : action.payload }
        default:
            return state;
    }
}