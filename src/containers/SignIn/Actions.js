import { 
    UPDATE_INPUT_SIGNIN,
    ON_LOADING_BUTTON,
    GO_TO_DASHBOARD 
} from "./types";

import * as firebase from "firebase";

export const updateInputSignIn = ({ prop, value }) => {
  return {
    type: UPDATE_INPUT_SIGNIN,
    payload: { prop, value }
  };
};

export const onSignIn = ({ email, password }) => {
  return dispatch => {

    handleDispatchLoading(dispatch, true);

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(user => {
          handleDispatchGoToDash(dispatch,true);
      })
      .catch(error => {
        handleDispatchLoading(dispatch, false);
      });
  };
};

const handleDispatchLoading = (dispatch, payload) => {
    dispatch({ 'type': ON_LOADING_BUTTON, 'payload': payload });
}

const handleDispatchGoToDash = (dispatch) => {
    dispatch({ 'type' : GO_TO_DASHBOARD, 'payload' : true})
}
