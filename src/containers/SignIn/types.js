/**
|--------------------------------------------------
| TYPES SIGNIN
|--------------------------------------------------
*/

export const UPDATE_INPUT_SIGNIN = 'update_input_signin';
export const ON_LOADING_BUTTON = 'on_loading_button';
export const ON_SIGNIN = 'on_SIGNIN';
export const GO_TO_DASHBOARD = 'go_to_dashboard';
