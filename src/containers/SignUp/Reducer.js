/**
|--------------------------------------------------
| REDUCER SIGNUP
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_SIGNUP,
    ON_LOADING_BUTTON,
    GO_TO_DASHBOARD
} from './types';

const INIT_STATE = {
    'name': '',
    'email': '',
    'password': '',
    'loadingButton': false,
    'goToDash': false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case UPDATE_INPUT_SIGNUP:
            return { ...state, [action.payload.prop]: action.payload.value }
        case ON_LOADING_BUTTON:
            return { ...state, loadingButton: action.payload }
        case GO_TO_DASHBOARD:
            return { ...state, goToDash: true }
        default:
            return state;
    }
}