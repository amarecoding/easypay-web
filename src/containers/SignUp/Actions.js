/**
|--------------------------------------------------
| ACTION CREATORS SIGN UP
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_SIGNUP,
    ON_LOADING_BUTTON,
    GO_TO_DASHBOARD
} from './types';

import * as firebase from 'firebase';

export const updateInputSignup = ({ prop, value }) => {
    return {
        'type': UPDATE_INPUT_SIGNUP,
        'payload': { prop, value }
    }
}

export const onCreateAccount = ({ name, email, password }) => {

    return (dispatch) => {
        handleDispatchLoading(dispatch, true);
        firebase.auth().createUserWithEmailAndPassword(email, password).then((user) => {
            if (user) {
                firebase.auth().onAuthStateChanged((miUser) => {
                    if (miUser) {
                        miUser.updateProfile({
                            displayName: name
                        }).then((response) => {
                            handleDispatchLoading(dispatch, false);
                            handleDispatchGoToDash(dispatch);
                        })
                    }
                })
            }
        }).catch((error) => {
            console.log(error);
            handleDispatchLoading(dispatch, false);
        })
    }
}

const handleDispatchLoading = (dispatch, payload) => {
    dispatch({ 'type': ON_LOADING_BUTTON, 'payload': payload });
}

const handleDispatchGoToDash = (dispatch) => {
    dispatch({ 'type': GO_TO_DASHBOARD, 'payload': true });
}