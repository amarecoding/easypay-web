import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Card } from 'antd';
import {
    updateInputSignup,
    onCreateAccount
} from './Actions';

const FormItem = Form.Item;

class SignUp extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const { name, email, password } = this.props;
                this.props.onCreateAccount({ name, email, password });
            }
        });
    }

    updateInputSignup = (prop, key) => {
        let value = key.target.value;
        this.props.updateInputSignup({ 'prop': prop, 'value': value });
    }

    onGoToDash = () => {
        if (this.props.goToDash) {
            setTimeout(() => {
                this.props.history.push('/dashboard');
            }, 100);
        }
    }

    render() {

        const { getFieldDecorator } = this.props.form;

        return (
            <div className="onlyFormContainer">
                <div className="onlyFormSubContainer">
                    <Card title="Registrarse" extra={<a href="/#">Iniciar Sesión</a>}>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <FormItem>
                                {getFieldDecorator('name', {
                                    rules: [{ required: true, message: 'Completa el campo nombre!' }],
                                    onChange: (e) => this.updateInputSignup('name', e)
                                })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Nombre" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: 'Completa el campo email!' }],
                                    onChange: (e) => this.updateInputSignup('email', e)
                                })(
                                    <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Complete el campo password!' }],
                                    onChange: (e) => this.updateInputSignup('password', e)
                                })(
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                )}
                            </FormItem>
                            <FormItem>
                                <Button type="primary" htmlType="submit" className="login-form-button" loading={this.props.loadingButton}>
                                    Registrarse
                                </Button>
                            </FormItem>
                            {this.onGoToDash()}
                        </Form>
                    </Card>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { name, email, password, loadingButton, goToDash } = state.SignUp;
    return { name, email, password, loadingButton, goToDash };
}


const WrappedSignUpForm = Form.create()(SignUp);
export default connect(mapStateToProps, {
    updateInputSignup,
    onCreateAccount
})(WrappedSignUpForm);