/**
|--------------------------------------------------
| TYPES SIGNUP
|--------------------------------------------------
*/
export const UPDATE_INPUT_SIGNUP = 'update_input_signup';
export const ON_LOADING_BUTTON = 'on_loading_button';
export const ON_CREATE_ACCOUNT = 'on_create_account';
export const GO_TO_DASHBOARD = 'go_to_dashboard';