/**
|--------------------------------------------------
| TYPES MENUFOOD
|--------------------------------------------------
*/

export const UPDATE_INPUT_MENUFOOD = "update_input_menufood";
export const ON_LOAD_CATEGORY_MENU = "on_load_category_menu";
export const ON_LOADING_MENUFOOD = 'on_loading_menufood';
export const ON_LOAD_MENU_FOOD = "on_load_menu_food";