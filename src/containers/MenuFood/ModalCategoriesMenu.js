import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input } from 'antd';

class ModalCategoriesMenu extends Component {

    render() {
        return (
            <div>
                <Modal
                    title="Editar Categoria"
                    cancelText="Cancelar"
                    visible={this.props.visible}
                    onCancel={this.props.handleCancel}
                    onOk={this.props.handleOk}>
                    <Input
                        placeholder="Escribe la categoria del platillo"
                        value={this.props.valueInput}
                        onChange={this.props.onModalUpdateInputNameCategory} />
                </Modal>
            </div>
        );
    }
}

ModalCategoriesMenu.propTypes = {
    handleCancel: PropTypes.func,
    handleOk: PropTypes.func,
    onModalUpdateInputNameCategory: PropTypes.func
}

export default ModalCategoriesMenu;