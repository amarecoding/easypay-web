import {
    UPDATE_INPUT_MENUFOOD,
    ON_LOAD_CATEGORY_MENU,
    ON_LOAD_MENU_FOOD,
    ON_LOADING_MENUFOOD
} from './types';

const INIT_STATE = {
    'categoryMenu': '',
    'saucer': '',
    'ingredients': '',
    'price': 0,
    'nameCategory': '',
    'categorysMenu': [],
    'menusFood': [],
    'loadingMenuFood': false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case UPDATE_INPUT_MENUFOOD:
            return { ...state, [action.payload.prop]: action.payload.value }
        case ON_LOADING_MENUFOOD:
            return { ...state, loadingMenuFood: action.payload }
        case ON_LOAD_CATEGORY_MENU:
            return { ...state, categorysMenu: action.payload }
        case ON_LOAD_MENU_FOOD:
            return { ...state, menusFood: action.payload }
        default:
            return state;
    }
}
