import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Tabs, Row, Col, Input, Button, Table, Divider, List, Popconfirm } from 'antd';
import {
    UpdateInputMenuFood,
    CreateCategoryMenu,
    UpdateCategoryMenu,
    onLoadCategorysMenu,
    DeleteCategoryMenu,
    onLoadMenusFood,
    DeleteMenuFood
} from './Actions'
import MenuFoodForm from '../MenuFood/MenuFoodForm';
import ModalCategoriesMenu from '../MenuFood/ModalCategoriesMenu';

const { Column } = Table;
const TabPane = Tabs.TabPane;

class MenuFood extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nameCategory: "",
            editNameCategory: "",
            keyNameCategory: "",
            visibleModal: false
        }
    }

    componentDidMount = () => {
        this.props.onLoadCategorysMenu();
        this.props.onLoadMenusFood();
    }

    onEditMenuFood = (row, e) => {
        this.childController.onLoadSetFields(row)
    }

    onDeleteMenuFood = (row, e) => {
        this.props.DeleteMenuFood(row);
    }

    //////////////////////////////////////////////////////////

    onUpdateInputNameCategory = (e) => {
        this.setState({ nameCategory: e.target.value });
    }

    onSaveNameCategory = () => {
        let nameCategory = this.state.nameCategory;
        this.props.CreateCategoryMenu({ nameCategory }).then(snapshot => {
            this.setState({ nameCategory: '' });
        }).catch(error => {
            console.log(error);
        })
    }

    onOpenModalEditNameCategory = (row, e) => {
        this.setState(
            {
                visibleModal: true,
                keyNameCategory: row.key,
                editNameCategory: row.nameCategory
            });
    }

    onModalUpdateInputNameCategory = (e) => {
        this.setState({ editNameCategory: e.target.value });
    }

    handleOk = () => {
        this.setState({ visibleModal: false });
        const key = this.state.keyNameCategory;
        this.props.UpdateCategoryMenu(key, { nameCategory: this.state.editNameCategory })
    }

    handleCancel = () => {
        this.setState({ visibleModal: false });
    }

    onDeleteNameCategory = (row) => {
        this.props.DeleteCategoryMenu(row);
    }

    render() {

        return (
            <div>
                <div className="main-container-con-margin">
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Ver mis menu" key="1">
                            <div>
                                <Row>
                                    <Col span={18}>
                                        <Table
                                            className="padding-table-form"
                                            dataSource={this.props.menusFood}
                                            loading={this.props.loading}
                                            locale={{ emptyText: 'Por el momento no existe ningun platillo.' }}>
                                            <Column
                                                title="Categoria"
                                                dataIndex="categoryMenu"
                                                key="categoryMenu"
                                            />
                                            <Column
                                                title="Platillo"
                                                dataIndex="saucer"
                                                key="saucer"
                                            />
                                            <Column
                                                title="Ingredientes"
                                                dataIndex="ingredients"
                                                key="ingredients"
                                            />
                                            <Column
                                                title="Precio"
                                                dataIndex="price"
                                                key="price"
                                            />
                                            <Column
                                                title="Acciones"
                                                key="actions"
                                                render={(text, row) => (
                                                    <span>
                                                        <a href="javascript:;" onClick={this.onEditMenuFood.bind(this, row)}>Editar</a>
                                                        <Divider type="vertical" />
                                                        <Popconfirm
                                                            title={`¿Quieres eliminar a ${row.saucer}?`} onConfirm={this.onDeleteMenuFood.bind(this, row)}
                                                            okText="Sí" cancelText="No" >
                                                            <a href="javascript:;">Eliminar</a>
                                                        </Popconfirm>
                                                    </span>
                                                )}
                                            />
                                        </Table>
                                    </Col>
                                    <Col span={6}>
                                        <MenuFoodForm
                                            provideController={controller => this.childController = controller}
                                            className="form-menu-food"
                                            isNewSau={true} />
                                    </Col>
                                </Row>
                            </div>
                        </TabPane>
                        <TabPane tab="Categorías" key="2">
                            <Row gutter={16}>
                                <Col span={8} className="gutter-row">
                                    <Input
                                        placeholder="Escribe la categoria del platillo"
                                        value={this.state.nameCategory}
                                        onChange={this.onUpdateInputNameCategory} />
                                </Col>
                                <Col span={8} className="gutter-row">
                                    <Button type="primary" onClick={this.onSaveNameCategory.bind(this)} >Crear</Button>
                                </Col>
                            </Row>
                            <br />
                            <ModalCategoriesMenu
                                visible={this.state.visibleModal}
                                handleOk={this.handleOk}
                                handleCancel={this.handleCancel}
                                onModalUpdateInputNameCategory={this.onModalUpdateInputNameCategory}
                                valueInput={this.state.editNameCategory} />
                            <List
                                dataSource={this.props.categorysMenu}
                                locale={{ emptyText: 'Por el momento no existe ningúna categoria para los platillos.' }}
                                bordered
                                renderItem={item => (
                                    <List.Item
                                        key={item.key}>
                                        <div className="action-module">
                                            <span>{item.nameCategory}</span>
                                            <a className="padding-left" onClick={this.onOpenModalEditNameCategory.bind(this, item)} href="javascript:;" >Editar</a>
                                            <Popconfirm
                                                title={`¿Quieres eliminar la categoria: ${item.nameCategory}?`} onConfirm={() => this.onDeleteNameCategory(item)}
                                                okText="Sí" cancelText="No" >
                                                <a href="javascript:;" >Eliminar</a>
                                            </Popconfirm>
                                        </div>
                                    </List.Item>
                                )}>
                            </List>
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { nameCategory, categorysMenu, menusFood, loadingMenuFood } = state.MenuFood;
    return { nameCategory, categorysMenu, menusFood, loadingMenuFood };
}

export default connect(mapStateToProps, {
    UpdateInputMenuFood,
    CreateCategoryMenu,
    UpdateCategoryMenu,
    onLoadCategorysMenu,
    DeleteCategoryMenu,
    onLoadMenusFood,
    DeleteMenuFood
})(MenuFood);