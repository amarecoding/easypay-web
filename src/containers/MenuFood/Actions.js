/**
|--------------------------------------------------
| ACTIONS CREATORS MENUFOOD
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_MENUFOOD,
    ON_LOAD_CATEGORY_MENU,
    ON_LOADING_MENUFOOD,
    ON_LOAD_MENU_FOOD
} from './types';

import * as Firebasehelper from '../../helpers/Firebasehelper';
import { onFormatCategorysMenu, onFormatMenusFood } from '../../helpers/FormatDocuments';
import firebase from 'firebase';

const collectionMenusFood = 'menusFood'
const collectionCategorysMenu = 'categoriesMenu';

export const UpdateInputMenuFood = ({ prop, value }) => {
    return {
        'type': UPDATE_INPUT_MENUFOOD,
        'payload': { prop, value }
    }
}

export const CreateCategoryMenu = ({ nameCategory }) => {

    return (dispatch) => {

        return new Promise((resolve, reject) => {
            Firebasehelper.Create(collectionCategorysMenu, { nameCategory }).then(snapshot => {
                resolve(snapshot);
            }).catch(error => {
                console.log(error);
                reject(error);
            });
        })
    }
}

export const UpdateCategoryMenu = (key, params) => {

    return (dispatch) => {

        Firebasehelper.Update(collectionCategorysMenu, key, params).then(snapshot => {
            console.log(snapshot);
        }).catch(error => {
            console.log(error);
        })
    }
}

export const DeleteCategoryMenu = (params) => {

    return (dispatch) => {

        let key = params.key;

        Firebasehelper.Delete(collectionCategorysMenu, key).then(snapshot => {
            console.log(snapshot);
        }).catch(error => {
            console.log(error);
        })

    }

}

export const onLoadCategorysMenu = () => {

    return (dispatch) => {

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true });

        db.collection(collectionCategorysMenu).onSnapshot((snapshot) => {
            let CategorysMenu = onFormatCategorysMenu(snapshot);
            dispatch({ type: ON_LOAD_CATEGORY_MENU, payload: CategorysMenu })
        }, (errror) => {
            console.log(errror)
        })
    }
}

/**
|--------------------------------------------------
| MENU FOOD ACTIONS CREATORS
|--------------------------------------------------
*/

export const CreateMenuFood = (params) => {

    return (dispatch) => {

        return new Promise((resolve, reject) => {

            Firebasehelper.Create(collectionMenusFood, params).then(snapshot => {
                resolve(snapshot);
            }).catch(error => {
                reject(error);
            })
        })
    }
}

export const EditMenuFood = (key, params) => {

    return (dispatch) => {

        return new Promise((resolve, reject) => {

            Firebasehelper.Update(collectionMenusFood, key, params).then(snapshot => {
                resolve(snapshot);
            }).catch(error => {
                reject(error);
            })

        })

    }
}

export const DeleteMenuFood = (params) => {

    return (dispatch) => {

        let key = params.key;

        Firebasehelper.Delete(collectionMenusFood, key).then(snapshot => {
            console.log(snapshot);
        }).catch(error => {
            console.log(error);
        })
    }
}

export const onLoadMenusFood = () => {
    return (dispatch) => {

        dispatch({ type: ON_LOADING_MENUFOOD, payload: true })

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true });

        db.collection(collectionMenusFood).onSnapshot((snapshot) => {
            let menusFood = onFormatMenusFood(snapshot);
            dispatch({ type: ON_LOAD_MENU_FOOD, payload: menusFood });
            dispatch({ type: ON_LOADING_MENUFOOD, payload: false })
        }, (error) => {
            console.log(error);
        })
    }
}