import React from 'react';
import { connect } from 'react-redux';
import { Form, Select, Input, Button } from 'antd';
import { UpdateInputMenuFood, CreateMenuFood, EditMenuFood } from './Actions';

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class MenuFoodForm extends React.Component {

    state = {
        formLayout: 'horizontal',
    };

    constructor(props) {
        super(props);
        this.state = {
            keyMenuFood: ""
        };
    }

    componentDidMount() {
        this.props.form.validateFields();
        this.props.provideController({
            onLoadSetFields: this.onLoadSetFields,
        })
    }

    componentWillUnmount() {
        this.props.provideController(null);
    }

    onLoadSetFields = (row) => {
        this.setState({ keyMenuFood: row.key });
        const { categoryMenu, saucer, ingredients, price } = row;
        this.props.form.setFieldsValue({ categoryMenu, saucer, ingredients, price });
    }

    handleSubmit = (e) => {

        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                const menuFood = this.onFormatDataMenuFood(values);

                if (this.state.keyMenuFood) {

                    let key = this.state.keyMenuFood;

                    this.props.EditMenuFood(key, menuFood).then(snapshot => {
                        console.log(snapshot);
                        this.props.form.resetFields()
                        this.setState({ keyMenuFood: "" });
                    }).catch(error => {
                        console.log(error);
                    })

                } else {

                    this.props.CreateMenuFood(menuFood).then(snapshot => {
                        this.props.form.resetFields()
                        this.setState({ keyMenuFood: "" });
                    }).catch(error => {
                        console.log(error);
                    })
                }

            } else {
                console.log('error');
            }
        });
    }

    onFormatDataMenuFood = (values) => {
        return {
            categoryMenu: values.categoryMenu,
            saucer: values.saucer,
            ingredients: values.ingredients,
            price: Number(values.price)
        };
    }

    UpdateInputMenuFood = (prop, key) => {
        let value = (key.target === undefined) ? key : key.target.value;
        this.props.UpdateInputMenuFood({ 'prop': prop, 'value': value });
    }

    onLoadOptionsSelect = () => {

        if (this.props.categorysMenu) {

            return this.props.categorysMenu.map((value, index) => {
                return (
                    <Option key={index} value={value.nameCategory}>{value.nameCategory}</Option>
                );
            });
        };
    }

    onClearInputForm = () => {
        this.props.form.resetFields()
        this.props.form.validateFields();
        this.setState({ keyMenuFood: "" });
    }

    render() {

        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
        const restaurantError = isFieldTouched('categoryMenu') && getFieldError('categoryMenu');
        const saucerError = isFieldTouched('saucer') && getFieldError('saucer');
        const ingredientsError = isFieldTouched('ingredients') && getFieldError('ingredients');
        const priceError = isFieldTouched('price') && getFieldError('price');

        const { formLayout } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 6 },
            wrapperCol: { span: 16 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
            wrapperCol: { span: 16, offset: 6 },
        } : null;

        return (

            <div className="form-menu-food">
                <Form layout={formLayout} onSubmit={this.handleSubmit}>

                    <FormItem
                        label="Categoría"
                        {...formItemLayout}
                        validateStatus={restaurantError ? 'error' : ''}
                        help={restaurantError || ''}>
                        {getFieldDecorator('categoryMenu', {
                            rules: [{ required: true, message: 'El campo categoría es requerido!' }],
                            onChange: (e) => this.UpdateInputMenuFood('categoryMenu', e),
                        })(
                            <Select>
                                {this.onLoadOptionsSelect()}
                            </Select>
                        )}

                    </FormItem>
                    <FormItem
                        label="Nombre del platillo"
                        {...formItemLayout}
                        validateStatus={saucerError ? 'error' : ''}
                        help={saucerError || ''}>
                        {getFieldDecorator('saucer', {
                            rules: [{ required: true, message: 'El campo nombre es requerido!' }],
                            onChange: (e) => this.UpdateInputMenuFood('saucer', e),
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem
                        label="Ingredientes"
                        {...formItemLayout}
                        validateStatus={ingredientsError ? 'error' : ''}
                        help={ingredientsError || ''}>
                        {getFieldDecorator('ingredients', {
                            rules: [{ required: true, message: 'El campo ingredientes es requerido!' }],
                            onChange: (e) => this.UpdateInputMenuFood('ingredients', e),
                        })(
                            <TextArea rows={3} />
                        )}
                    </FormItem>
                    <FormItem
                        label="Precio"
                        {...formItemLayout}
                        validateStatus={priceError ? 'error' : ''}
                        help={priceError || ''}>
                        {getFieldDecorator('price', {
                            rules: [{ required: true, message: 'El campo precio es requerido!' }],
                            onChange: (e) => this.UpdateInputMenuFood('price', e),
                        })(
                            <Input type="number" addonBefore="$" />
                        )}
                    </FormItem>
                    <FormItem {...buttonItemLayout}>
                        <Button
                            className="margin-right"
                            type="primary"
                            htmlType="submit"
                            disabled={hasErrors(getFieldsError())}>
                            {this.props.isNewSau ? 'Guardar' : 'Editar'}
                        </Button>

                        <Button
                            htmlType="button"
                            onClick={this.onClearInputForm.bind(this)}>
                            Cancelar
                        </Button>
                    </FormItem>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    const { categoryMenu, saucer,
        ingredients, price, categorysMenu } = state.MenuFood;

    return {
        categoryMenu, saucer,
        ingredients, price, categorysMenu
    };
}

const WrappedNewMenuFoodForm = Form.create()(MenuFoodForm);

export default connect(mapStateToProps, {
    UpdateInputMenuFood,
    CreateMenuFood,
    EditMenuFood
}, null, { withRef: true })(WrappedNewMenuFoodForm);