/**
|--------------------------------------------------
| ACTION CREATORS RESTAURANTE
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_RESTAURANT,
    LIST_ALL_RESTAURANT,
    LOADING_RESTAURANT
} from './types';

import { onFormatNotification, Notification } from '../../components/ListComponents';
import { onFormatListRest } from '../../helpers/FormatDocuments';
import * as Firebasehelper from '../../helpers/Firebasehelper';
import firebase from 'firebase';

const collection = 'restaurants';

export const updateInputRestaurant = ({ prop, value }) => {
    return {
        'type': UPDATE_INPUT_RESTAURANT,
        'payload': { prop, value }
    }
}

export const createRestaurant = ({ name, direction, telephone }) => {

    return (dispatch) => {

        return new Promise((resolve, reject) => {

            Firebasehelper.Create(collection,
                { name, direction, telephone }).then(snapshot => {
                    let formatNoti = onFormatNotification('Pefecto', 'Haz creado un nuevo restaurante....', true)
                    Notification(formatNoti);
                    resolve(snapshot);
                }).catch(error => {
                    reject(error);
                });
        })
    }
}

export const updateRestaurant = (key, params) => {

    return (dispatch) => {

        return new Promise((resolve, reject) => {

            Firebasehelper.Update(collection, key, params).then(snapshot => {
                let formatNoti = onFormatNotification('Pefecto', 'Haz editado un restaurante....', true)
                Notification(formatNoti);
                resolve(formatNoti);
            }).catch(error => {
                reject(error);
            })

        })
    }
}

export const deleteRestaurant = (props) => {

    return (dispatch) => {

        let key = props.key

        Firebasehelper.Delete(collection, key).then(snapshot => {
            console.log(snapshot);
        }).catch(error => {
            console.log(error);
        })
    }
}

export const onListRestaurant = () => {

    return (dispatch) => {

        dispatch({ type: LOADING_RESTAURANT, payload: true });

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true });

        db.collection(collection).onSnapshot((snapshot) => {
            let restaurants = onFormatListRest(snapshot)
            dispatch({ type: LIST_ALL_RESTAURANT, payload: restaurants });
            dispatch({ type: LOADING_RESTAURANT, payload: false });
        }, (errror) => {
            console.log(errror)
        })
    }
}

export const onGetRestaurant = (keydocument) => {

    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Firebasehelper.Get(collection, keydocument).then(snapshot => {
                if (snapshot.exists) {
                    resolve(snapshot.data());
                }
            }).catch(error => {
                reject(error);
            })
        });
    }
}