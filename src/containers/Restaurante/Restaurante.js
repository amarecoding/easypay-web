import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Drawer, Popconfirm, Divider, Button, Tooltip } from 'antd';
import { onListRestaurant, deleteRestaurant } from './Actions';
import RestauranteForm from './RestauranteForm';

const { Column } = Table;

class Restaurante extends Component {

    constructor(props) {
        super(props)
        this.state = {
            view: "",
            visible: false,
            isNewRest: true
        };
    }

    componentDidMount = () => {
        this.props.onListRestaurant();
    }

    onDeleteRestaurant = (row, e) => {
        this.props.deleteRestaurant(row);
    }

    showDrawerNewRest = () => {
        this.setState({ visible: true, isNewRest: true, view: "Culo" });
        setTimeout(() => {
            this.childController.onClearForm()
        }, 100)
    };

    showDrawerEditRest = (row, e) => {
        this.setState({ visible: true, isNewRest: false });
        setTimeout(() => {
            this.childController.onLoadSetFields(row)
        }, 100)
    };

    onCloseDrawer = () => {
        this.setState({ visible: false });
    }

    render() {
        return (
            <div>
                <div className="main-container-con-margin">
                    <div className="action-module">
                        <p>Lista de Restaurantes.</p>
                        <Tooltip placement="left" title="Crear nuevo restaurante">
                            <Button className="margin-botton-1em" type="primary" shape="circle" icon="plus" onClick={this.showDrawerNewRest.bind(this)} />
                        </Tooltip>
                    </div>
                    <Table
                        dataSource={this.props.restaurants}
                        loading={this.props.loading}
                        locale={{ emptyText: 'Por el momento no existe ningún restaurante registrado.' }}>
                        <Column
                            title="Nombre"
                            dataIndex="name"
                            key="name" />
                        <Column
                            title="Dirección"
                            dataIndex="direction"
                            key="direction" />
                        <Column
                            title="Teléfono"
                            dataIndex="telephone"
                            key="telephone" />
                        <Column
                            title="Acciones"
                            key="actions"
                            render={(text, row) => (
                                <span>
                                    <a href="javascript:;" onClick={this.showDrawerEditRest.bind(this, row)}>Editar</a>
                                    <Divider type="vertical" />
                                    <Popconfirm
                                        title={`¿Quieres eliminar a ${row.name}?`} onConfirm={this.onDeleteRestaurant.bind(this, row)}
                                        okText="Sí" cancelText="No" >
                                        <a href="javascript:;">Eliminar</a>
                                    </Popconfirm>
                                </span>
                            )} />
                    </Table>
                    <Drawer
                        title={this.state.isNewRest ? "Nuevo Restaurante" : "Editar Restaurante"}
                        placement="right"
                        closable={false}
                        width={640}
                        onClose={this.onCloseDrawer}
                        visible={this.state.visible}>
                        <RestauranteForm
                            isNewRest={this.state.isNewRest}
                            provideController={controller => this.childController = controller} />
                    </Drawer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { loading, restaurants } = state.Restaurante;
    return { loading, restaurants };
}

export default connect(mapStateToProps, {
    onListRestaurant,
    deleteRestaurant
})(Restaurante);