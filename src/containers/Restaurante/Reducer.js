/**
|--------------------------------------------------
| REDUCERS RESTAURANTE
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_RESTAURANT,
    LIST_ALL_RESTAURANT,
    LOADING_RESTAURANT,
} from './types';

const INIT_STATE = {
    name: '',
    direction: '',
    telephone: '',
    loading: false,
    restaurants: [],
    objRest: {}
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case UPDATE_INPUT_RESTAURANT:
            return { ...state, [action.payload.prop]: action.payload.value };
        case LOADING_RESTAURANT:
            return { ...state, loading: action.payload };
        case LIST_ALL_RESTAURANT:
            return { ...state, restaurants: action.payload };
        default:
            return state;
    }

}