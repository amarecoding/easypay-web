import React from 'react';
import { connect } from 'react-redux';
import {
    updateInputRestaurant, createRestaurant,
    updateRestaurant, onGetRestaurant
} from './Actions';
import { Form, Input, Button } from 'antd';
const FormItem = Form.Item;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class RestauranteForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            formLayout: 'horizontal',
            keydocument: null
        };
    }

    componentDidMount = () => {
        this.onClearForm()
        this.props.provideController({
            onClearForm: this.onClearForm,
            onLoadSetFields: this.onLoadSetFields,
        })
    }

    componentWillUnmount() {
        this.props.provideController(null);
    }

    onClearForm = () => {
        this.onClearForm();
    }

    onLoadSetFields = (row) => {
        if (row) {
            const { name, direction, telephone } = row;
            this.setState({ keydocument: row.key })
            this.props.form.setFieldsValue({ name, direction, telephone });
        }
    }

    handleSubmit = (e) => {

        e.preventDefault();
        this.props.form.validateFields((err, values) => {

            if (!err) {

                const { name, direction, telephone } = values;

                if (this.props.isNewRest) {
                    console.log('cree')
                    this.props.createRestaurant({ name, direction, telephone }).then(snapshot => {
                        console.log(snapshot);
                        this.onClearForm();
                    }).catch(error => {
                        console.log(error);
                    })
                } else {
                    console.log('edite')
                    let key = this.state.keydocument;
                    if (key) {
                        console.log('edite 2')
                        this.props.updateRestaurant(key, { name, direction, telephone }).then(snapshot => {
                            console.log(snapshot);
                        }).catch(error => {
                            console.log(error)
                        })
                    }

                }
            }
        });
    }

    updateInputRestaurant = (prop, key) => {
        let value = key.target.value;
        this.props.updateInputRestaurant({ 'prop': prop, 'value': value });
    }

    onClearForm = () => {
        this.props.form.resetFields()
        this.props.form.validateFields();
    }

    render() {

        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
        const nameError = isFieldTouched('name') && getFieldError('name');
        const directionError = isFieldTouched('direction') && getFieldError('direction');
        const telephoneError = isFieldTouched('telephone') && getFieldError('telephone');

        const { formLayout } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 4 },
            wrapperCol: { span: 16 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
            wrapperCol: { span: 16, offset: 4 },
        } : null;

        return (

            <div>

                <p className="margin-left">Completa todos los campos.</p>

                <Form layout={formLayout} onSubmit={this.handleSubmit}>
                    <FormItem
                        label="Nombre"
                        {...formItemLayout}
                        validateStatus={nameError ? 'error' : ''}
                        help={nameError || ''}>
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'El campo nombre es requerido!' }],
                            onChange: (e) => this.updateInputRestaurant('name', e),
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem
                        label="Dirección"
                        {...formItemLayout}
                        validateStatus={directionError ? 'error' : ''}
                        help={directionError || ''}>
                        {getFieldDecorator('direction', {
                            rules: [{ required: true, message: 'El campo dirección es requerido!' }],
                            onChange: (e) => this.updateInputRestaurant('direction', e)
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem
                        label="Teléfono"
                        {...formItemLayout}
                        validateStatus={telephoneError ? 'error' : ''}
                        help={telephoneError || ''}>
                        {getFieldDecorator('telephone', {
                            rules: [{ required: true, message: 'El campo teléfono es requerido!' }],
                            onChange: (e) => this.updateInputRestaurant('telephone', e)
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...buttonItemLayout}>
                        <Button
                            type="primary"
                            htmlType="submit"
                            disabled={hasErrors(getFieldsError())}>
                            {this.props.isNewRest ? 'Registrar' : 'Editar'}
                        </Button>
                    </FormItem>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { name, direction, telephone, objRest } = state.Restaurante;
    return { name, direction, telephone, objRest };
}

const WrappedNewResrForm = Form.create()(RestauranteForm);

export default connect(mapStateToProps, {
    updateInputRestaurant,
    createRestaurant,
    onGetRestaurant,
    updateRestaurant
}, null, { withRef: true })(WrappedNewResrForm);