/**
|--------------------------------------------------
| TYPES RESTAURANTE
|--------------------------------------------------
*/

export const UPDATE_INPUT_RESTAURANT = 'update_input_restaurante';
export const CREATE_UPDATE_RESTAURANTE = 'create_update_restaurante';
export const LIST_ALL_RESTAURANT = 'list_all_restaurant';
export const DELETE_RESTAURANT = 'delete_restaurant';
export const LOADING_RESTAURANT = 'loading_restaurant';
