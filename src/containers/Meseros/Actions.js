/**
|--------------------------------------------------
| ACTION CREATORS MESEROS
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_MESEROS,
    ON_LOAD_RESTAURANTS,
    ON_LOAD_WAITERS,
    LOADING_WAITERS
} from './types';

import { onFormatNotification, Notification } from '../../components/ListComponents';
import * as Firebasehelper from '../../helpers/Firebasehelper';
import { onFormatListWaiters, onFormatListRest } from '../../helpers/FormatDocuments';
import firebase from 'firebase';

const collectionrestaurant = 'restaurants';
const collectionwaiters = 'waiters';

export const updateInputMeseros = ({ prop, value }) => {
    return {
        'type': UPDATE_INPUT_MESEROS,
        'payload': { prop, value }
    }
}

export const onListWaiters = () => {

    return (dispatch) => {

        dispatch({ type: LOADING_WAITERS, payload: true });

        let db = firebase.firestore();
        db.settings({ timestampsInSnapshots: true });

        db.collection(collectionwaiters).onSnapshot((snapshot) => {

            let waiters = onFormatListWaiters(snapshot);
            dispatch({ type: ON_LOAD_WAITERS, payload: waiters });
            dispatch({ type: LOADING_WAITERS, payload: false });

        }, (error) => {
            console.log(error)
        });
    }
}

export const onListRestaurant = () => {

    return (dispatch) => {

        Firebasehelper.List(collectionrestaurant).then((snapshot) => {
            let restaurants = onFormatListRest(snapshot);
            dispatch({ type: ON_LOAD_RESTAURANTS, payload: restaurants });
        });
    }
}

export const createWaiter = ({ name, restaurant, tableNumber }) => {

    return (dispatch) => {

        Firebasehelper.Create(collectionwaiters,
            { name, restaurant, tableNumber }).then(snapshot => {
                let formatNoti = onFormatNotification('Pefecto', 'Haz creado un nuevo mesero....', true)
                Notification(formatNoti);
            }).catch(error => {
                console.log(error);
            });
    }
}

export const updateWaiter = (key, params) => {

    return (dispatch) => {
        Firebasehelper.Update(collectionwaiters, key, params).then(snapshot => {
            let formatNoti = onFormatNotification('Pefecto', 'Haz editado un mesero....', true)
            Notification(formatNoti);
        }).catch(error => {
            console.log(error);
        });
    }
}

export const deleteWaiter = (props) => {

    return (dispatch) => {

        let key = props.key

        Firebasehelper.Delete(collectionwaiters, key).then(snapshot => {
            console.log(snapshot);
        }).catch(error => {
            console.log(error);
        })
    }
}