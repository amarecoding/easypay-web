import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    updateInputMeseros, onListRestaurant,
    createWaiter,
    updateWaiter
} from './Actions';
import { Form, Input, Button, Select } from 'antd';

const Option = Select.Option;
const FormItem = Form.Item;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class MeserosForm extends Component {

    state = {
        formLayout: 'horizontal',
        keydocument: null
    };

    constructor(props) {
        super(props);
        this.onLoadOptionsSelect = this.onLoadOptionsSelect.bind(this);
    }

    componentDidMount() {
        this.props.onListRestaurant();
        this.props.provideController({
            onClearForm: this.onClearForm,
            onLoadSetFields: this.onLoadSetFields,
        })
    }

    componentWillUnmount() {
        this.props.provideController(null);
    }

    onClearForm = () => {
        this.onClearForm();
    }

    onLoadSetFields = (row) => {
        if (row) {
            const { name, restaurant, tableNumber } = row;
            this.setState({ keydocument: row.key })
            this.props.form.setFieldsValue({ name, restaurant, tableNumber });
        }
    }

    onLoadOptionsSelect = () => {

        if (this.props.restaurants) {

            return this.props.restaurants.map((value, index) => {
                return (
                    <Option key={index} value={value.name}>{value.name}</Option>
                );
            });
        };
    }

    updateInputMeseros = (prop, key) => {
        let value = (key.target === undefined) ? key : key.target.value;
        this.props.updateInputMeseros({ 'prop': prop, 'value': value });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                const { name, restaurant, tableNumber } = values;

                if (this.props.isNewWaiter) {
                    this.props.createWaiter({ name, restaurant, tableNumber });
                    this.onClearForm();
                } else {
                    let keydocument = this.state.keydocument;
                    this.props.updateWaiter(keydocument, { name, restaurant, tableNumber });
                }
            }
        });
    }

    onClearForm = () => {
        this.props.form.resetFields()
        this.props.form.validateFields();
    }

    render() {

        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
        const nameError = isFieldTouched('name') && getFieldError('name');
        const restaurantError = isFieldTouched('restaurant') && getFieldError('restaurant');
        const tableNumberError = isFieldTouched('tableNumber') && getFieldError('tableNumber');

        const { formLayout } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 6 },
            wrapperCol: { span: 16 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
            wrapperCol: { span: 16, offset: 6 },
        } : null;

        return (

            <div>

                <p className="margin-left">Completa todos los campos.</p>

                <Form layout={formLayout} onSubmit={this.handleSubmit}>
                    <FormItem
                        label="Nombre"
                        {...formItemLayout}
                        validateStatus={nameError ? 'error' : ''}
                        help={nameError || ''}>
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'El campo nombre es requerido!' }],
                            onChange: (e) => this.updateInputMeseros('name', e),
                        })(
                            <Input />
                        )}
                    </FormItem>

                    <FormItem
                        label="Restaurante"
                        {...formItemLayout}
                        validateStatus={restaurantError ? 'error' : ''}
                        help={restaurantError || ''}>
                        {getFieldDecorator('restaurant', {
                            rules: [{ required: true, message: 'El campo restaurante es requerido!' }],
                            onChange: (e) => this.updateInputMeseros('restaurant', e),
                        })(
                            <Select>
                                {this.onLoadOptionsSelect()}
                            </Select>
                        )}

                    </FormItem>

                    <FormItem
                        label="Numero de Mesa"
                        {...formItemLayout}
                        validateStatus={tableNumberError ? 'error' : ''}
                        help={tableNumberError || ''}>
                        {getFieldDecorator('tableNumber', {
                            rules: [{ required: true, message: 'El campo numero de tabla es requerido!' }],
                            onChange: (e) => this.updateInputMeseros('tableNumber', e)
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...buttonItemLayout}>
                        <Button
                            type="primary"
                            htmlType="submit"
                            disabled={hasErrors(getFieldsError())}>
                            {this.props.isNewWaiter ? 'Registrar' : 'Editar'}
                        </Button>
                    </FormItem>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { name, restaurant, tableNumber, restaurants } = state.Meseros;
    return { name, restaurant, tableNumber, restaurants };
}

const WrappedNewMeseroForm = Form.create()(MeserosForm);

export default connect(mapStateToProps, {
    updateInputMeseros,
    onListRestaurant,
    createWaiter,
    updateWaiter
}, null, { withRef: true })(WrappedNewMeseroForm);