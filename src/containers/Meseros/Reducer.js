/**
|--------------------------------------------------
| REDUCER Meseros
|--------------------------------------------------
*/

import {
    UPDATE_INPUT_MESEROS,
    ON_LOAD_RESTAURANTS,
    LOADING_WAITERS,
    ON_LOAD_WAITERS
} from './types';

const INIT_STATE = {
    'name': '',
    'restaurant': '',
    'tableNumber': '',
    'loading': false,
    'restaurants': [],
    'waiters': []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case UPDATE_INPUT_MESEROS:
            return { ...state, [action.payload.prop]: action.payload.value };
        case ON_LOAD_RESTAURANTS:
            return { ...state, restaurants: action.payload };
        case LOADING_WAITERS:
            return { ...state, loading: action.payload };
        case ON_LOAD_WAITERS:
            return { ...state, waiters: action.payload };
        default:
            return state;
    }
}