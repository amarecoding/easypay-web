import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Drawer, Popconfirm, Divider, Button, Tooltip } from 'antd';
import { onListWaiters, deleteWaiter } from './Actions';

import MeserosForm from './MeserosForm';

const { Column } = Table;

class Meseros extends Component {

    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            isNewWaiter: true
        };
    }

    componentWillMount = () => {
        this.props.onListWaiters();
    }

    onNewWaiter = () => {
        this.setState({ visible: true, isNewWaiter: true });
        setTimeout(() => {
            this.waiterController.onClearForm()
        }, 100)
    }

    onEditWaiter = (row, e) => {
        this.setState({ visible: true, isNewWaiter: false });
        setTimeout(() => {
            this.waiterController.onLoadSetFields(row)
        }, 100)
    }

    onDeleteWaiter = (row) => {
        this.props.deleteWaiter(row);
    }

    onCloseDrawer = () => {
        this.setState({ visible: false });
    }

    render() {
        return (
            <div>
                <div className="main-container-con-margin">
                    <div className="action-module">
                        <p>Lista de Meseros.</p>
                        <Tooltip placement="left" title="Crear nuevo mesero">
                            <Button className="margin-botton-1em" type="primary" shape="circle" icon="plus" onClick={this.onNewWaiter.bind(this)} />
                        </Tooltip>
                    </div>
                    <Table
                        dataSource={this.props.waiters}
                        loading={this.props.loading}
                        locale={{ emptyText: 'Por el momento no existe ningún mesero registrado.' }}>
                        <Column
                            title="Nombre"
                            dataIndex="name"
                            key="name"
                        />
                        <Column
                            title="Restaurante"
                            dataIndex="restaurant"
                            key="restaurant"
                        />
                        <Column
                            title="Numero Mesa"
                            dataIndex="tableNumber"
                            key="tableNumber"
                        />
                        <Column
                            title="Acciones"
                            key="actions"
                            render={(text, row) => (
                                <span>
                                    <a href="javascript:void(0)" onClick={(e) => this.onEditWaiter(row, e)}>Editar</a>
                                    <Divider type="vertical" />
                                    <Popconfirm
                                        title={`¿Quieres eliminar a ${row.name}?`} onConfirm={() => this.onDeleteWaiter(row)}
                                        okText="Sí" cancelText="No" >
                                        <a href="javascript:void(0)">Eliminar</a>
                                    </Popconfirm>
                                </span>
                            )}
                        />
                    </Table>
                    <Drawer
                        title={this.state.isNewWaiter ? "Nuevo Mesero" : "Editar Mesero"}
                        placement="right"
                        closable={false}
                        width={640}
                        onClose={this.onCloseDrawer}
                        visible={this.state.visible}>
                        <MeserosForm
                            isNewWaiter={this.state.isNewWaiter}
                            provideController={controller => this.waiterController = controller}
                            objForm={this.state.objForm} />
                    </Drawer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { loading, waiters } = state.Meseros;
    return { loading, waiters };
}

export default connect(mapStateToProps, {
    onListWaiters,
    deleteWaiter
})(Meseros);
