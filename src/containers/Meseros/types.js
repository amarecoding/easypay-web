/**
|--------------------------------------------------
| TYPES RESTAURANTE
|--------------------------------------------------
*/

export const UPDATE_INPUT_MESEROS = 'update_input_meseros';
export const ON_LOAD_RESTAURANTS = 'on_load_restaurants';
export const ON_LOAD_WAITERS = 'on_load_waiters';
export const LOADING_WAITERS = 'loading_waiters';