import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink, HashRouter } from "react-router-dom";
import { Layout, Menu, Icon, Avatar, Dropdown } from 'antd';

import SignIn from './containers/SignIn/SignIn';
import SignUp from './containers/SignUp/SignUp';
import Dashboard from './containers/Dashboard/Dashboard';
import Restaurante from './containers/Restaurante/Restaurante';
import Mesas from './containers/Mesas/Mesas';
import Meseros from './containers/Meseros/Meseros';
import MenuFood from './containers/MenuFood/MenuFood';

import { CheckLoginUser } from './helpers/Firebasehelper';
import { SetRootRoute, PrivateRoute } from './stateless/MyRoutes';

const { Header, Content, Footer, Sider } = Layout;

const menu = (
    <Menu>
        <Menu.Item key="0">
            <Icon type="area-chart" /> Metas del dia
        </Menu.Item>
        <Menu.Item key="1">
            <Icon type="setting" /> Perfil
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">
            <Icon type="logout" /> Salir
        </Menu.Item>
    </Menu>
);

class SideBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view: "Dashboard",
            collapsed: true,
            isLoginUser: false
        };
    }

    componentDidMount = () => {
        CheckLoginUser().then((user) => {
            if (user) {
                this.setState({ isLoginUser: true })
            }
        })
    }

    onCollapse = (collapsed) => {
        this.setState({ collapsed });
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    handleClickMenu = (e) => {
        this.setState({ 'currentView': e.key });
    }

    changeViewName = (view) => {
        this.setState({ view: view })
    }

    render() {

        const isLoginUser = this.state.isLoginUser

        return (
            <HashRouter>
                <Layout>
                    {isLoginUser && (
                        <Sider
                            collapsible
                            collapsed={this.state.collapsed}
                            onCollapse={this.onCollapse}>
                            <div className="logo" />
                            <Menu className="full-height-sidebar"
                                theme="light"
                                defaultSelectedKeys={['dashboard']}
                                mode="inline">
                                <Menu.Item key="dashboard">
                                    <Icon type="line-chart" />
                                    <span>Dashboard</span>
                                    <NavLink to="/dashboard" onClick={this.changeViewName.bind(this, "Dashboard")} />
                                </Menu.Item>
                                <Menu.Item key="restaurante">
                                    <Icon type="shop" />
                                    <span>Restaurante</span>
                                    <NavLink to="/restaurante" onClick={this.changeViewName.bind(this, "Restaurante")} />
                                </Menu.Item>
                                <Menu.Item key="mesas">
                                    <Icon type="schedule" />
                                    <span>Mesas</span>
                                    <NavLink to="/mesas" onClick={this.changeViewName.bind(this, "Mesas")} />
                                </Menu.Item>
                                <Menu.Item key="meseros">
                                    <Icon type="user" />
                                    <span>Meseros</span>
                                    <NavLink to="/meseros" onClick={this.changeViewName.bind(this, "Meseros")} />
                                </Menu.Item>
                                <Menu.Item key="menu">
                                    <Icon type="profile" />
                                    <span>Menu</span>
                                    <NavLink to="/menu" onClick={this.changeViewName.bind(this, "Menu")} />
                                </Menu.Item>
                            </Menu>
                        </Sider>
                    )}

                    <Layout>
                        {isLoginUser && (
                            <Header className="contentSubHeader">
                                <div className="spaceContentHeader">
                                    <div>
                                        <Icon
                                            className="trigger"
                                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                            onClick={this.toggle} />
                                        <strong>{this.state.view}</strong>
                                    </div>
                                    <div>
                                        <Dropdown overlay={menu}>
                                            <a className="ant-dropdown-link">
                                                <Avatar icon="user" />
                                            </a>
                                        </Dropdown>
                                    </div>
                                </div>
                            </Header>
                        )}
                        <Content>
                            <SetRootRoute exact path="/" component={SignIn} isLoginUser={isLoginUser} />
                            <SetRootRoute path="/registrar" component={SignUp} isLoginUser={isLoginUser} />
                            <PrivateRoute path="/dashboard" component={Dashboard} isLoginUser={isLoginUser} />
                            <PrivateRoute path="/restaurante" component={Restaurante} isLoginUser={isLoginUser} />
                            <PrivateRoute path="/mesas" component={Mesas} isLoginUser={isLoginUser} />
                            <PrivateRoute path="/meseros" component={Meseros} isLoginUser={isLoginUser} />
                            <PrivateRoute path="/menu" component={MenuFood} isLoginUser={isLoginUser} />
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>Power by easypay ©2018 Creado con mucho <span role="img" aria-label="love">💖</span> {isLoginUser} </Footer>
                    </Layout>
                </Layout>
            </HashRouter>
        );
    }
}

SideBar.propTypes = {
    callaction: PropTypes.func
}

export default SideBar;