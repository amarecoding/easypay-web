
import React from 'react';
import { notification, Icon } from 'antd';

export const onFormatNotification = (title, description, isSuccess) => {
    return {
        title: title,
        description: description,
        isSuccess: isSuccess
    }
}

export const Notification = (props) => {

    const { title, description } = props;
    const icon = props.isSuccess ? 'check' : 'close';
    const color = props.isSuccess ? '#1B5E20' : '#D50000';

    notification.open({
        message: title,
        description: description,
        icon: <Icon type={icon} style={{ color: color }} />,
    });
}