import React from 'react';
import { Route, Redirect } from "react-router-dom";

export const SetRootRoute = ({ component: Component, isLoginUser, ...rest }) => (
    <Route
        {...rest}
        render={(props) => isLoginUser === true ? (

            <Redirect
                to={{
                    pathname: "/dashboard",
                    state: { from: props.location }
                }}
            />

        ) : (
                <Component {...props} />
            )
        }
    />
);

export const PrivateRoute = ({ component: Component, isLoginUser, ...rest }) => (
    <Route
        {...rest}
        render={(props) => isLoginUser === true ? (
            <Component {...props} />
        ) : (
                <Redirect
                    to={{
                        pathname: "/",
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
);