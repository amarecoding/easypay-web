import React from 'react';
import { Link } from "react-router-dom";
import { Icon } from 'antd';

export const NavBreadcrumb = (props) => {

    const list = props.list;

    return (
        <div>
            <ul className="menu-navegacion">
                <li key={1}>
                    <Link to="/dashboard">
                        <Icon type="home" /> /
                    </Link>
                </li>
                {
                    list.map((value, index) => {

                        if (list.length === (index + 1)) {
                            return (
                                <li key={value.key}>
                                    <span> {value.text}</span>
                                </li>
                            )
                        } else {
                            return (
                                <li key={value.key}>
                                    <Link to={value.href}> <span> {value.text} / </span> </Link>
                                </li>
                            )
                        }
                    })
                }
            </ul>
        </div >
    );
}