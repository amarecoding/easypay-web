import { combineReducers } from 'redux';
import SignInReducer from '../containers/SignIn/Reducer';
import SignUpReducer from '../containers/SignUp/Reducer';
import RestauranteReducer from '../containers/Restaurante/Reducer';
import MesasReducer from '../containers/Mesas/Reducer';
import MeserosReducer from '../containers/Meseros/Reducer';
import MenuFood from '../containers/MenuFood/Reducer';

export default combineReducers({
    SignIn: SignInReducer,
    SignUp: SignUpReducer,
    Restaurante: RestauranteReducer,
    Mesas: MesasReducer,
    Meseros: MeserosReducer,
    MenuFood: MenuFood
});